//
//  ListViewModel.swift
//  ToDoList
//
//  Created by Daniel Siancas on 18/01/21.
//

import SwiftUI
import RealmSwift

class ListViewModel: ObservableObject {
    @Published var title = ""
    @Published var detail = ""
    @Published var openNewPage = false
    @Published var items: [Item] = []
    @Published var updateObject: Item?
    
    init() {
        fetchData()
    }
    
    func fetchData() {
        guard let dbRef = try? Realm() else { return }
        
        let results = dbRef.objects(Item.self)
        
        self.items = results.compactMap { $0 }
    }
    
    func addData(presentation: Binding<PresentationMode>) {
        let item = Item()
        item.title = title
        item.detail = detail
        
        guard let dbRef = try? Realm() else { return }
        
        try? dbRef.write {
            guard let availableObject = updateObject else {
                dbRef.add(item)
                return
            }
            
            availableObject.title = title
            availableObject.detail = detail
        }
        
        fetchData()
        
        presentation.wrappedValue.dismiss()
    }
    
    func deleteData(object: Item) {
        guard let dbRef = try? Realm() else { return }
        
        try? dbRef.write {
            dbRef.delete(object)
            fetchData()
        }
    }
    
    func setupInitialData() {
        guard let updateData = updateObject else { return }
        
        title = updateData.title
        detail = updateData.detail
    }
    
    func deInitData() {
        updateObject = nil
        title = ""
        detail = ""
    }
}
