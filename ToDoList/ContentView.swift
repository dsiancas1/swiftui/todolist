//
//  ContentView.swift
//  ToDoList
//
//  Created by Daniel Siancas on 18/01/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Home()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
