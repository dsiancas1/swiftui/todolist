//
//  Homr.swift
//  ToDoList
//
//  Created by Daniel Siancas on 18/01/21.
//

import SwiftUI

struct Home: View {
    @StateObject var modelData = ListViewModel()
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(spacing: 15) {
                    ForEach(modelData.items) { item in
                        VStack(alignment: .leading, spacing: 10, content: {
                            Text(item.title)
                            Text(item.detail)
                                .font(.caption)
                                .foregroundColor(.gray)
                        })
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(10)
                        .background(Color.gray.opacity(0.15))
                        .cornerRadius(10)
                        .contentShape(RoundedRectangle(cornerRadius: 10))
                        .contextMenu(menuItems: {
                            Button(action: { modelData.deleteData(object: item) } , label: {
                                Text("Delete Item")
                            })
                            Button(action: {
                                modelData.updateObject = item
                                modelData.openNewPage.toggle()
                            }, label: {
                                Text("Update Item")
                            })
                        })
                    }
                }
                .padding()
            }
            .navigationTitle("To-Do List")
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {modelData.openNewPage.toggle()}) {
                        Image(systemName: "plus")
                            .font(.title2)
                    }
                }
            }
            .sheet(isPresented: $modelData.openNewPage, content: {
                AddPageView()
                    .environmentObject(modelData)
            })
        }
    }
}

struct Homr_Previews: PreviewProvider {
    static var previews: some View {
        Home()
    }
}
