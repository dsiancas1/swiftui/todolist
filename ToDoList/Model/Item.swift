//
//  Item.swift
//  ToDoList
//
//  Created by Daniel Siancas on 18/01/21.
//

import Foundation
import RealmSwift

class Item: Object, Identifiable {
    @objc dynamic var id: Date = Date()
    @objc dynamic var title = ""
    @objc dynamic var detail = ""
}
