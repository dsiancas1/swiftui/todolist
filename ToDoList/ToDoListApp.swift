//
//  ToDoListApp.swift
//  ToDoList
//
//  Created by Daniel Siancas on 18/01/21.
//

import SwiftUI

@main
struct ToDoListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
